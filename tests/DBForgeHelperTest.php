<?php
namespace Tests;

use MyDBForge;
use PHPUnit\Framework\TestCase;

class DBForgeHelperTest extends TestCase
{

    /**
     * @dataProvider commentAfterFirstProvider
     * @return void
     */
    public function testAddFirst($fieldData)
    {
        $real = MyDBForge\DBForgeHelper::first($fieldData);
        $expected = array_merge($fieldData, ['first' => true]);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider commentAfterFirstProvider
     * @return void
     */
    public function testAddAfter($fieldData)
    {
        $fakeFieldName = 'Foo bar';
        $real = MyDBForge\DBForgeHelper::after($fieldData, $fakeFieldName);
        $expected = array_merge($fieldData, ['after' => $fakeFieldName]);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider commentAfterFirstProvider
     * @return void
     */
    public function testAddComment($fieldData)
    {
        $fakeComment = 'Foo bar';
        $real = MyDBForge\DBForgeHelper::comment($fieldData, $fakeComment);
        $expected = array_merge($fieldData, ['comment' => "'$fakeComment'"]);
        $this->assertEquals($expected, $real);
    }

    /**
     *
     * @dataProvider binaryFieldProvider
     * @return void
     */
    public function testBinaryField($testingParams, $expectedParams)
    {
        $nullableParam = $testingParams["null"] ?? null;
        $defaultParam = $testingParams["default"] ?? null;
        $real = MyDBForge\DBForgeHelper::binaryField($nullableParam, $defaultParam);
        $common = [
            'type' => "TINYINT",
            'constraint' => 1,
            'unsigned' => true,
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider bigintFieldProvider
     * @return void
     */
    public function testBigintField($testingParams, $expectedParams)
    {
        $sizeParam = $testingParams["constraint"] ?? null;
        $nullableParam = $testingParams["null"] ?? null;
        $defaultParam = $testingParams["default"] ?? null;
        $real = MyDBForge\DBForgeHelper::bigintField($sizeParam, $nullableParam, $defaultParam);
        $common = [
            'type' => "BIGINT",
            'unsigned' => true,
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider intFieldProvider
     * @return void
     */
    public function testIntField($testingParams, $expectedParams)
    {
        $sizeParam = $testingParams["constraint"] ?? null;
        $nullableParam = $testingParams["null"] ?? null;
        $defaultParam = $testingParams["default"] ?? null;
        $real = MyDBForge\DBForgeHelper::intField($sizeParam, $nullableParam, $defaultParam);
        $common = [
            'type' => "INT",
            'unsigned' => true,
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider decimalFieldProvider
     * @return void
     */
    public function testDecimalField($testingParams, $expectedParams)
    {
        $sizeParam = $testingParams["constraint"] ?? null;
        $nullableParam = $testingParams["null"] ?? null;
        $defaultParam = $testingParams["default"] ?? null;
        $real = MyDBForge\DBForgeHelper::decimalField($sizeParam, $nullableParam, $defaultParam);
        $common = [
            'type' => "DECIMAL",
            'unsigned' => true,
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider varcharFieldProvider
     * @return void
     */
    public function testVarcharField($testingParams, $expectedParams)
    {
        $sizeParam = $testingParams["constraint"] ?? null;
        $nullableParam = $testingParams["null"] ?? null;
        $defaultParam = $testingParams["default"] ?? null;
        $real = MyDBForge\DBForgeHelper::varcharField($sizeParam, $nullableParam, $defaultParam);
        $common = [
            'type' => "VARCHAR",
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider textFieldProvider
     * @return void
     */
    public function testTextField($testingParams, $expectedParams)
    {
        $nullableParam = $testingParams["null"] ?? null;
        $defaultParam = $testingParams["default"] ?? null;
        $real = MyDBForge\DBForgeHelper::textField($nullableParam, $defaultParam);
        $common = [
            'type' => "TEXT",
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider dateFieldProvider
     * @return void
     */
    public function testDateField($testingParams, $expectedParams)
    {
        $nullableParam = $testingParams["null"] ?? null;
        $defaultParam = $testingParams["default"] ?? null;
        $real = MyDBForge\DBForgeHelper::dateField($nullableParam, $defaultParam);
        $common = [
            'type' => "DATE",
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider datetimeFieldProvider
     * @return void
     */
    public function testDatetimeField($testingParams, $expectedParams)
    {
        $nullableParam = $testingParams["null"] ?? null;
        $defaultParam = $testingParams["default"] ?? null;
        $real = MyDBForge\DBForgeHelper::datetimeField($nullableParam, $defaultParam);
        $common = [
            'type' => "DATETIME",
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider timestampFieldProvider
     * @return void
     */
    public function testTimestampField($testingParams, $expectedParams)
    {
        $nullableParam = $testingParams["null"] ?? null;
        $defaultParam = $testingParams["default"] ?? null;
        $real = MyDBForge\DBForgeHelper::timestampField($nullableParam, $defaultParam);
        $common = [
            'type' => "TIMESTAMP",
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }
    public function testCurrentTimestampField()
    {
        $real = MyDBForge\DBForgeHelper::currenttimestampField();
        $expected = [
            'type' => "TIMESTAMP",
            'null' => false,
            'default' => 'CURRENT_TIMESTAMP()',
        ];

        $expected = array_merge($real, $expected);
        $this->assertEquals($expected, $real);
    }

    /**
     *
     * @dataProvider enumFieldProvider
     * @return void
     */
    public function testEnumField($testingParams, $expectedParams)
    {
        $valuesParam = $testingParams["constraint"] ?? null;
        $nullableParam = $testingParams["null"] ?? null;
        $defaultParam = $testingParams["default"] ?? null;
        $real = MyDBForge\DBForgeHelper::enumField($valuesParam, $nullableParam, $defaultParam);
        $common = [
            'type' => "ENUM",
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }
    /**
     *
     * @dataProvider pkFieldProvider
     * @return void
     */
    public function testPkField($testingParams, $expectedParams)
    {
        $sizeParam = $testingParams["constraint"] ?? null;

        $real = MyDBForge\DBForgeHelper::pkField($sizeParam);
        $common = [
            'type' => "INT",
            'unsigned' => true,
            'auto_increment' => true,
        ];

        $expected = array_merge($common, $expectedParams);
        $this->assertEquals($expected, $real);
    }

    /**
     * Undocumented function
     * @return void
     */
    public function testEnumFieldThrownExceptionBecauseNotConstraintIsSent()
    {

        if (PHP_VERSION_ID >= 70100) {
            $this->expectException(\ArgumentCountError::class);
        } else {
            $this->expectException(PHPUnit_Framework_Error_Warning::class);
        }
        MyDBForge\DBForgeHelper::enumField();
    }

    //----------------------------------------------------------------------------
    // PROVIDERS
    //----------------------------------------------------------------------------
    public function commentAfterFirstProvider()
    {
        return [
            [MyDBForge\DBForgeHelper::pkField()],
            [MyDBForge\DBForgeHelper::intField()],
            [MyDBForge\DBForgeHelper::bigintField()],
            [MyDBForge\DBForgeHelper::varcharField()],
            [MyDBForge\DBForgeHelper::textField()],
            [MyDBForge\DBForgeHelper::enumField(['active', 'pending'])],
            [MyDBForge\DBForgeHelper::binaryField()],
        ];

    }
    public function bigintFieldProvider()
    {
        //Testing all params are null (default values)
        $testingDefaultValues = [];
        $expectedDefaultValues = [
            'constraint' => 20,
            'null' => true,
            'default' => null,
        ];

        //Testing all custom
        $testingAllCustom = ['constraint' => 5, 'null' => false, 'default' => 25];
        $expectedAllCustom = $testingAllCustom;
        //Testing size Null
        $testingConstraintNull = ['null' => false, 'default' => 18];
        $expectedConstraintNull = array_merge($expectedDefaultValues, $testingConstraintNull);
        //Testing 'null' is Null
        $testingNullableNull = ['constraint' => 7, 'default' => 15];
        $expectedNullableNull = array_merge($expectedDefaultValues, $testingNullableNull);
        //Testing 'default' is Null
        $testingDefaultNull = ['constraint' => 3, 'null' => false];
        $expectedDefaultNull = array_merge($expectedDefaultValues, $testingDefaultNull);

        return [
            [$testingAllCustom, $expectedAllCustom],
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingConstraintNull, $expectedConstraintNull],
            [$testingNullableNull, $expectedNullableNull],
            [$testingDefaultNull, $expectedDefaultNull],
        ];
    }
    public function binaryFieldProvider()
    {
        //Testing all params are null (default values)
        $testingDefaultValues = [];
        $expectedDefaultValues = [
            'constraint' => 1,
            'null' => true,
            'default' => null,
        ];

        //Testing all custom
        $testingAllCustom = ['null' => false, 'default' => 25];
        $expectedAllCustom = $testingAllCustom;

        //Testing 'null' is Null
        $testingNullableNull = ['default' => 1];
        $expectedNullableNull = array_merge($expectedDefaultValues, $testingNullableNull);
        //Testing 'default' is Null
        $testingDefaultNull = ['null' => false];
        $expectedDefaultNull = array_merge($expectedDefaultValues, $testingDefaultNull);

        return [
            [$testingAllCustom, $expectedAllCustom],
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingNullableNull, $expectedNullableNull],
            [$testingDefaultNull, $expectedDefaultNull],
        ];
    }
    public function intFieldProvider()
    {
        //Testing all params are null (default values)
        $testingDefaultValues = [];
        $expectedDefaultValues = [
            'constraint' => 10,
            'null' => true,
            'default' => null,
        ];

        //Testing all custom
        $testingAllCustom = ['constraint' => 5, 'null' => false, 'default' => 25];
        $expectedAllCustom = $testingAllCustom;
        //Testing size Null
        $testingConstraintNull = ['null' => false, 'default' => 18];
        $expectedConstraintNull = array_merge($expectedDefaultValues, $testingConstraintNull);
        //Testing 'null' is Null
        $testingNullableNull = ['constraint' => 7, 'default' => 15];
        $expectedNullableNull = array_merge($expectedDefaultValues, $testingNullableNull);
        //Testing 'default' is Null
        $testingDefaultNull = ['constraint' => 3, 'null' => false];
        $expectedDefaultNull = array_merge($expectedDefaultValues, $testingDefaultNull);

        return [
            [$testingAllCustom, $expectedAllCustom],
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingConstraintNull, $expectedConstraintNull],
            [$testingNullableNull, $expectedNullableNull],
            [$testingDefaultNull, $expectedDefaultNull],
        ];
    }

    public function decimalFieldProvider()
    {
        //Testing all params are null (default values)
        $testingDefaultValues = [];
        $expectedDefaultValues = [
            'constraint' => "10,2",
            'null' => true,
            'default' => null,
        ];

        //Testing all custom
        $testingAllCustom = ['constraint' => "5,3", 'null' => false, 'default' => 25];
        $expectedAllCustom = $testingAllCustom;
        //Testing size Null
        $testingConstraintNull = ['null' => false, 'default' => 18];
        $expectedConstraintNull = array_merge($expectedDefaultValues, $testingConstraintNull);
        //Testing 'null' is Null
        $testingNullableNull = ['constraint' => "2,1", 'default' => 15];
        $expectedNullableNull = array_merge($expectedDefaultValues, $testingNullableNull);
        //Testing 'default' is Null
        $testingDefaultNull = ['constraint' => "3,2", 'null' => false];
        $expectedDefaultNull = array_merge($expectedDefaultValues, $testingDefaultNull);

        return [
            [$testingAllCustom, $expectedAllCustom],
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingConstraintNull, $expectedConstraintNull],
            [$testingNullableNull, $expectedNullableNull],
            [$testingDefaultNull, $expectedDefaultNull],
        ];
    }

    public function varcharFieldProvider()
    {
        //Testing all params are null (default values)
        $testingDefaultValues = [];
        $expectedDefaultValues = [
            'constraint' => 255,
            'null' => true,
            'default' => null,
        ];

        //Testing all custom
        $testingAllCustom = ['constraint' => 5, 'null' => false, 'default' => 25];
        $expectedAllCustom = $testingAllCustom;
        //Testing size Null
        $testingConstraintNull = ['null' => false, 'default' => 18];
        $expectedConstraintNull = array_merge($expectedDefaultValues, $testingConstraintNull);
        //Testing 'null' is Null
        $testingNullableNull = ['constraint' => 7, 'default' => 15];
        $expectedNullableNull = array_merge($expectedDefaultValues, $testingNullableNull);
        //Testing 'default' is Null
        $testingDefaultNull = ['constraint' => 3, 'null' => false];
        $expectedDefaultNull = array_merge($expectedDefaultValues, $testingDefaultNull);

        return [
            [$testingAllCustom, $expectedAllCustom],
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingConstraintNull, $expectedConstraintNull],
            [$testingNullableNull, $expectedNullableNull],
            [$testingDefaultNull, $expectedDefaultNull],
        ];
    }

    public function pkFieldProvider()
    {
        //Testing all params are null (default values)
        $testingDefaultValues = [];
        $expectedDefaultValues = [
            'constraint' => 10,
        ];

        //Testing size Null
        $testingConstraintNull = ['constraint' => 20];
        $expectedConstraintNull = array_merge($expectedDefaultValues, $testingConstraintNull);

        return [
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingConstraintNull, $expectedConstraintNull],
        ];
    }
    public function textFieldProvider()
    {
        //Testing all params are null (default values)
        $testingDefaultValues = [];
        $expectedDefaultValues = [
            'null' => true,
            'default' => null,
        ];

        //Testing all custom
        $testingAllCustom = ['null' => false, 'default' => 'foo'];
        $expectedAllCustom = $testingAllCustom;

        //Testing 'null' is Null
        $testingNullableNull = ['default' => 'bar'];
        $expectedNullableNull = array_merge($expectedDefaultValues, $testingNullableNull);
        //Testing 'default' is Null
        $testingDefaultNull = ['null' => false];
        $expectedDefaultNull = array_merge($expectedDefaultValues, $testingDefaultNull);

        return [
            [$testingAllCustom, $expectedAllCustom],
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingNullableNull, $expectedNullableNull],
            [$testingDefaultNull, $expectedDefaultNull],
        ];
    }
    public function timestampFieldProvider()
    {
        //Testing all params are null (default values)
        $testingDefaultValues = [];
        $expectedDefaultValues = [
            'null' => true,
            'default' => null,
        ];
        $now = time();
        //Testing all custom
        $testingAllCustom = ['null' => false, 'default' => $now];
        $expectedAllCustom = $testingAllCustom;

        //Testing 'null' is Null
        $testingNullableNull = ['default' => $now];
        $expectedNullableNull = array_merge($expectedDefaultValues, $testingNullableNull);
        //Testing 'default' is Null
        $testingDefaultNull = ['null' => false];
        $expectedDefaultNull = array_merge($expectedDefaultValues, $testingDefaultNull);

        return [
            [$testingAllCustom, $expectedAllCustom],
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingNullableNull, $expectedNullableNull],
            [$testingDefaultNull, $expectedDefaultNull],
        ];
    }
    public function dateFieldProvider()
    {
        //Testing all params are null (default values)
        $testingDefaultValues = [];
        $expectedDefaultValues = [
            'null' => true,
            'default' => null,
        ];

        //Testing all custom
        $testingAllCustom = ['null' => false, 'default' => '2019-03-03'];
        $expectedAllCustom = $testingAllCustom;

        //Testing 'null' is Null
        $testingNullableNull = ['default' => '2019-04-03'];
        $expectedNullableNull = array_merge($expectedDefaultValues, $testingNullableNull);
        //Testing 'default' is Null
        $testingDefaultNull = ['null' => false];
        $expectedDefaultNull = array_merge($expectedDefaultValues, $testingDefaultNull);

        return [
            [$testingAllCustom, $expectedAllCustom],
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingNullableNull, $expectedNullableNull],
            [$testingDefaultNull, $expectedDefaultNull],
        ];
    }
    public function datetimeFieldProvider()
    {
        //Testing all params are null (default values)
        $testingDefaultValues = [];
        $expectedDefaultValues = [
            'null' => true,
            'default' => null,
        ];

        //Testing all custom
        $testingAllCustom = ['null' => false, 'default' => '2019-03-03 12:00:00'];
        $expectedAllCustom = $testingAllCustom;

        //Testing 'null' is Null
        $testingNullableNull = ['default' => '2019-04-03 12:00:00'];
        $expectedNullableNull = array_merge($expectedDefaultValues, $testingNullableNull);
        //Testing 'default' is Null
        $testingDefaultNull = ['null' => false];
        $expectedDefaultNull = array_merge($expectedDefaultValues, $testingDefaultNull);

        return [
            [$testingAllCustom, $expectedAllCustom],
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingNullableNull, $expectedNullableNull],
            [$testingDefaultNull, $expectedDefaultNull],
        ];
    }

    public function enumFieldProvider()
    {
        //Testing all params are null -except constraint which is mandatory- (default values)
        $fakeValues = ['foo', 'bar'];
        $testingDefaultValues = [
            'constraint' => $fakeValues,
        ];

        $expectedDefaultValues = [
            'null' => true,
            'default' => null,
            'constraint' => $fakeValues,
        ];

        //Testing all custom
        $testingAllCustom = ['constraint' => $fakeValues, 'null' => false, 'default' => 'foo'];
        $expectedAllCustom = $testingAllCustom;

        //Testing 'null' is Null
        $testingNullableNull = ['constraint' => $fakeValues, 'default' => 'bar'];
        $expectedNullableNull = array_merge($expectedDefaultValues, $testingNullableNull);
        //Testing 'default' is Null
        $testingDefaultNull = ['constraint' => $fakeValues, 'null' => false];
        $expectedDefaultNull = array_merge($expectedDefaultValues, $testingDefaultNull);

        return [
            [$testingAllCustom, $expectedAllCustom],
            [$testingDefaultValues, $expectedDefaultValues],
            [$testingNullableNull, $expectedNullableNull],
            [$testingDefaultNull, $expectedDefaultNull],
        ];
    }

}
