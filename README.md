Helper methods to use in Codeigniter 4 Framework at Migration Module. Provide the follows methods:

* pkField()
* intField()
* varcharField()
* textField()
* enumField()
* binaryField()
* decimalField() (since 1.0.9)