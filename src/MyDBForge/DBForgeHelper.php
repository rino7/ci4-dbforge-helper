<?php
namespace MyDBForge;

class DBForgeHelper
{
    public function __construct()
    {
        //$forge = \Config\Database::forge();
    }

    /**
     * En lugar de borrar la tabla, la renombra para borrarla luego manualmente
     *
     * @param string $table
     * @return void
     */
    public static function safeDropTable($table)
    {
        $forge = \Config\Database::forge();
        $forge->renameTable($table, '_tbr_since' . date('YmdHis') . '_' . $table);
    }

    /**
     * En lugar de borrar la columna, la renombra para borrarla luego manualmente
     *
     * @param string $table
     * @param string $column
     * @return void
     */
    public static function safeDropColumn($table, $column)
    {
        $forge = \Config\Database::forge();
        $db = \Config\Database::connect();
        $fields = $db->getFieldData($table);

        foreach ($fields as $field) {
            if ($column === $field->name) {
                $metaData = $field;
                break;
            }
        }
        if (empty($metaData)) {
            throw new \Exception('No se ha encontrado la columna a eliminar');
        }
        $newValues = [
            $column => [
                'name' => '_ tbr_since' . date('YmdHis') . '_' . $column,
                'type' => $metaData->type,
                'constraint' => $metaData->max_length,
                'default' => $metaData->default,
                'comment' => "'Marcada para borrar el: " . date('d/m/Y H:i:s') . "'",
            ],
        ];
        $forge->modifyColumn($table, $newValues);
    }

    public static function pkField(int $size = null)
    {
        return [
            'type' => 'INT',
            'constraint' => $size ?? 10,
            'unsigned' => true,
            'auto_increment' => true,
        ];

    }
    public static function binaryField(bool $nullable = null, int $default = null)
    {
        return [
            'type' => 'TINYINT',
            'constraint' => 1,
            'unsigned' => true,
            'null' => $nullable === false ? false : true,
            'default' => $default,
        ];
    }

    public static function bigintField(int $size = null, bool $nullable = null, int $default = null)
    {
        return [
            'type' => 'BIGINT',
            'constraint' => $size ?? 20,
            'unsigned' => true,
            'null' => $nullable === false ? false : true,
            'default' => $default,
        ];
    }

    public static function intField(int $size = null, bool $nullable = null, int $default = null)
    {
        return [
            'type' => 'INT',
            'constraint' => $size ?? 10,
            'unsigned' => true,
            'null' => $nullable === false ? false : true,
            'default' => $default,
        ];
    }
    /**
     * Devuelve el array de valores para generar una columna decimal(10,2)
     *
     * @param string $size El size del decimal [Def.: 10,2]
     * @param boolean $nullable Si puede ser null o no
     * @param integer $default Cuál es el valor por default en caso de no poder ser null
     * @return array
     */
    public static function decimalField(string $size = null, bool $nullable = null, int $default = null)
    {
        return [
            'type' => 'DECIMAL',
            'constraint' => $size ?? "10,2",
            'unsigned' => true,
            'null' => $nullable === false ? false : true,
            'default' => $default,
        ];
    }

    public static function varcharField(int $size = null, bool $nullable = null, string $default = null)
    {
        return [
            'type' => 'VARCHAR',
            'constraint' => $size ?? 255,
            'null' => $nullable === false ? false : true,
            'default' => $default,
        ];
    }
    public static function textField(bool $nullable = null, string $default = null)
    {
        return [
            'type' => 'TEXT',
            'null' => $nullable === false ? false : true,
            'default' => $default,
        ];
    }
    public static function timestampField(bool $nullable = null, string $default = null)
    {
        return [
            'type' => 'TIMESTAMP',
            'null' => $nullable === false ? false : true,
            'default' => $default,
        ];
    }

    public static function currentTimestampField()
    {
        return self::timestampField(false, 'CURRENT_TIMESTAMP()');
    }

    public static function dateField(bool $nullable = null, string $default = null)
    {
        return [
            'type' => 'DATE',
            'null' => $nullable === false ? false : true,
            'default' => $default,
        ];
    }
    public static function datetimeField(bool $nullable = null, string $default = null)
    {
        return [
            'type' => 'DATETIME',
            'null' => $nullable === false ? false : true,
            'default' => $default,
        ];
    }

    public static function enumField(array $values, bool $nullable = null, string $default = null)
    {
        return [
            'type' => 'ENUM',
            'constraint' => $values, //['publish', 'pending', 'draft']
            'null' => $nullable === false ? false : true,

            'default' => $default, //pending
        ];
    }

    /**
     * Agrega el key 'comment' al array de datos del field
     *
     * @param array $fieldData El array de datos del field
     * @return array El nuevo array de datos de la columna con el ítme 'comment' => "'$comment'" agregado
     * @return array
     */
    public static function comment(array $fieldData, string $comment)
    {
        $newFieldData = $fieldData;
        $newFieldData['comment'] = "'{$comment}'";
        return $newFieldData;
    }
    /**
     * Agrega el key 'after' al array de datos del field para indicar
     * después de qué columna existente queremos agregar la nueva columna
     *
     * @param array $fieldData El array de datos del field
     * @param string $comment La columna existente luego de la cual se agregará la nueva
     * @return array El nuevo array de datos de la columna con el ítme 'after' => $afterField agregado
     */
    public static function after(array $fieldData, string $afterField)
    {
        $newFieldData = $fieldData;
        $newFieldData['after'] = $afterField;
        return $newFieldData;
    }
    /**
     * Agrega el key 'first' = TRUE al array de datos del field para indicar
     * que la columna debe ser la primera en la tabla
     *
     * @param array $fieldData El array de datos del field
     * @return array El nuevo array de datos de la columna con el ítme 'first' => TRUE agregado
     */
    public static function first(array $fieldData)
    {
        $newFieldData = $fieldData;
        $newFieldData['first'] = true;
        return $newFieldData;
    }
}
